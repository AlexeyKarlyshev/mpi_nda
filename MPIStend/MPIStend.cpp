﻿#include <mpi.h>
#include <iostream>
#include "windows.h"
#include <queue>
#include <random>
#include <algorithm>
#include <iterator>
#include <clocale>
#include <vector>
#include <map>
#include <string>

//КАРТА ПЕРЕХОДОВ АВТОМАТА НДКА
std::map<std::string, std::vector<int>> routes = { { "e01", {0,1} },{ "e12", {1,2} },{ "e21", {2,1} },{ "e13", {1,3} },{ "e31", {3,1} } };

//ФЛАГ БЛОКИРОВКИ ВЕРШИНЫ
bool isLock = false;

//ФУНКЦИЯ ПЕРЕХОДА В СЛЕДУЮЩУЮ ВЕРШИНУ, пример: GoToVertex(RANK, "e23") при условии что RANK == 2;
void GoToVertex(int RANK, std::string edge_or_key)
{
	std::map<std::string, std::vector<int>>::iterator it = routes.find(edge_or_key);
	if (it != routes.end() && RANK == it->second[0])
	{
		int op = 1;
		MPI_Request request;
		MPI_Send(&op, 1, MPI_INT, it->second[1], 0, MPI_COMM_WORLD);
	}
}
int main(int argc, char** argv)
{
	setlocale(LC_ALL, "Russian");
	int RANK, SIZE, selectedRank = -1;
	byte code = 0; //текущее состояние автомата;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &SIZE);
	std::cout << "SIZE = " << SIZE << std::endl;
	MPI_Comm_rank(MPI_COMM_WORLD, &RANK);
	std::cout << "RANK = " << RANK << std::endl;
	MPI_Barrier(MPI_COMM_WORLD); //синхронизация всех потоков
	code = 0; //инициальное событие автомата

	while (1)
	{
		if (RANK == 0)
		{
			//КОД ВЕРШИНЫ
			GoToVertex(RANK, "e01");
			break;
		}
		else if (RANK == 1)
		{
			//КОД ВЕРШИНЫ
			int message;
			MPI_Status st;
			MPI_Recv(&message, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &st);
			std::cout << message << std::endl;
		}
		else if (RANK == 2)
		{
			//КОД ВЕРШИНЫ

		}
		else if (RANK == 3)
		{
			//КОД ВЕРШИНЫ

		}
	}
	MPI_Finalize(); //???
	return 0;
}
//"C:\Program Files\Microsoft HPC Pack 2008 SDK\Bin\mpiexec" -n 4 MPIStend.exe