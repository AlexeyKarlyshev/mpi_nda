﻿namespace SystAnalys_lr1
{
    partial class VertexProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.vertex_name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.vertex_pcode = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // vertex_name
            // 
            this.vertex_name.Location = new System.Drawing.Point(73, 12);
            this.vertex_name.Name = "vertex_name";
            this.vertex_name.ReadOnly = true;
            this.vertex_name.Size = new System.Drawing.Size(100, 20);
            this.vertex_name.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Вершина:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Код для вершины:";
            // 
            // vertex_pcode
            // 
            this.vertex_pcode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.vertex_pcode.Location = new System.Drawing.Point(15, 51);
            this.vertex_pcode.Name = "vertex_pcode";
            this.vertex_pcode.Size = new System.Drawing.Size(423, 213);
            this.vertex_pcode.TabIndex = 0;
            this.vertex_pcode.Text = "";
            // 
            // VertexProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(450, 283);
            this.Controls.Add(this.vertex_pcode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.vertex_name);
            this.Name = "VertexProperties";
            this.Text = "VertexProperties";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VertexProperties_FormClosing);
            this.Load += new System.EventHandler(this.VertexProperties_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox vertex_name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox vertex_pcode;
    }
}