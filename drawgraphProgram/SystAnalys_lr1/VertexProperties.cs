﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SystAnalys_lr1
{
    public partial class VertexProperties : Form
    {
        private Vertex vertex;
        public VertexProperties()
        {
            InitializeComponent();
        }

        public VertexProperties(Vertex vertex) : this()
        {
            this.vertex = vertex;
        }

        private void VertexProperties_Load(object sender, EventArgs e)
        {
            if (vertex != null) 
            {
                vertex_name.Text = vertex.name;
                vertex_pcode.Text = vertex.pcode;
            }
        }

        private void VertexProperties_FormClosing(object sender, FormClosingEventArgs e)
        {
            vertex.pcode = vertex_pcode.Text;
        }
    }
}
