﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;

namespace SystAnalys_lr1
{
    public partial class Form1 : Form
    {
        DrawGraph G;
        List<Vertex> V;
        List<Edge> E;
        int[,] AMatrix; //матрица смежности
        int[,] IMatrix; //матрица инцидентности

        int selected1, selected2; //выбранные вершины, для соединения линиями

        public Form1()
        {
            InitializeComponent();
            V = new List<Vertex>();
            G = new DrawGraph(sheet.Width, sheet.Height);
            E = new List<Edge>();
            sheet.Image = G.GetBitmap();
        }

        private void UpdateButtons(bool isSelected, bool isVertex, bool isEdge, bool isDelete) 
        {
            selectButton.Enabled = isSelected;
            drawVertexButton.Enabled = isVertex;
            drawEdgeButton.Enabled = isEdge;
            deleteButton.Enabled = isDelete;
        }

        //кнопка - выбрать вершину
        private void selectButton_Click(object sender, EventArgs e)
        {
            UpdateButtons(false, true, true, true);
            G.clearSheet();
            G.drawALLGraph(V, E);
            sheet.Image = G.GetBitmap();
            selected1 = -1;
        }

        //кнопка - рисовать вершину
        private void drawVertexButton_Click(object sender, EventArgs e)
        {
            UpdateButtons(true, false, true, true);
            G.clearSheet();
            G.drawALLGraph(V, E);
            sheet.Image = G.GetBitmap();
        }

        //кнопка - рисовать ребро
        private void drawEdgeButton_Click(object sender, EventArgs e)
        {
            UpdateButtons(true, true, false, true);
            G.clearSheet();
            G.drawALLGraph(V, E);
            sheet.Image = G.GetBitmap();
            selected1 = -1;
            selected2 = -1;
        }

        //кнопка - удалить элемент
        private void deleteButton_Click(object sender, EventArgs e)
        {
            UpdateButtons(true, true, true, false);
            G.clearSheet();
            G.drawALLGraph(V, E);
            sheet.Image = G.GetBitmap();
        }

        //кнопка - удалить граф
        private void deleteALLButton_Click(object sender, EventArgs e)
        {
            UpdateButtons(true, true, true, true);
            if (MessageBox.Show("Вы действительно хотите полностью удалить граф?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                V.Clear();
                E.Clear();
                G.clearSheet();
                sheet.Image = G.GetBitmap();
            }
        }

        //кнопка - матрица смежности
        private void buttonAdj_Click(object sender, EventArgs e) => createAdjAndOut();

        //кнопка - матрица инцидентности 
        private void buttonInc_Click(object sender, EventArgs e) => createIncAndOut();

        private void sheet_MouseClick(object sender, MouseEventArgs e)
        {
            //нажата кнопка "выбрать вершину", ищем степень вершины
            if (selectButton.Enabled == false)
            {
                for (int i = 0; i < V.Count; i++)
                {
                    if (Math.Pow((V[i].x - e.X), 2) + Math.Pow((V[i].y - e.Y), 2) <= G.R * G.R)
                    {
                        if (selected1 != -1)
                        {
                            selected1 = -1;
                            G.clearSheet();
                            G.drawALLGraph(V, E);
                            sheet.Image = G.GetBitmap();
                        }
                        if (selected1 == -1)
                        {
                            G.drawSelectedVertex(V[i].x, V[i].y);
                            selected1 = i;
                            sheet.Image = G.GetBitmap();
                            createAdjAndOut();
                            listBoxMatrix.Items.Clear();
                            int degree = 0;
                            for (int j = 0; j < V.Count; j++)
                                degree += AMatrix[selected1, j];
                            listBoxMatrix.Items.Add($"Степень вершины №{(selected1 + 1).ToString()} равна {degree.ToString()}");
                            if (e.Button == MouseButtons.Right)
                                new VertexProperties(V[i]).ShowDialog();
                            break;
                        }
                    }
                }
            }
            //нажата кнопка "рисовать вершину"
            if (drawVertexButton.Enabled == false)
            {
                var vertex = new Vertex(e.X, e.Y, V.Count.ToString());
                V.Add(vertex);
                G.drawVertex(vertex.x, vertex.y, vertex.name);
                sheet.Image = G.GetBitmap();
            }
            //нажата кнопка "рисовать ребро"
            if (drawEdgeButton.Enabled == false)
            {
                if (e.Button == MouseButtons.Left)
                {
                    for (int i = 0; i < V.Count; i++)
                    {
                        if (Math.Pow((V[i].x - e.X), 2) + Math.Pow((V[i].y - e.Y), 2) <= G.R * G.R)
                        {
                            if (selected1 == -1)
                            {
                                G.drawSelectedVertex(V[i].x, V[i].y);
                                selected1 = i;
                                sheet.Image = G.GetBitmap();
                                break;
                            }
                            if (selected2 == -1)
                            {
                                G.drawSelectedVertex(V[i].x, V[i].y);
                                selected2 = i;
                                var ExistEdge = E.FirstOrDefault(x => (x.v1 == selected1 && x.v2 == selected2) || (x.v1 == selected2 && x.v2 == selected1));
                                Edge edge = null;
                                if (ExistEdge == null) //если вообще не создана вершина
                                {
                                    edge = new Edge(selected1, V[selected1].name, selected2, V[selected2].name);
                                    E.Add(edge);
                                    G.drawEdge(V[selected1], V[selected2], edge);
                                }
                                else //если уже создана то следующие сценарии:
                                {
                                    //если нет реверса, пробуем создать:
                                    if (ExistEdge.CreateReverse(selected1, selected2)) //если реверс создался, мы обновляем его
                                    {
                                        G.clearSheet();
                                        G.drawALLGraph(V, E);
                                    }
                                }
                                selected1 = -1;
                                selected2 = -1;
                                sheet.Image = G.GetBitmap();
                                break;
                            }
                        }
                    }
                }
                else if (e.Button == MouseButtons.Right)
                {
                    if ((selected1 != -1) && (Math.Pow((V[selected1].x - e.X), 2) + Math.Pow((V[selected1].y - e.Y), 2) <= G.R * G.R))
                    {
                        G.drawVertex(V[selected1].x, V[selected1].y, (selected1 + 1).ToString());
                        selected1 = -1;
                        sheet.Image = G.GetBitmap();
                    }
                }
            }
            //нажата кнопка "удалить элемент"
            if (deleteButton.Enabled == false)
            {
                bool flag = false; //удалили ли что-нибудь по ЭТОМУ клику
                //ищем, возможно была нажата вершина
                for (int i = 0; i < V.Count; i++)
                {
                    if (Math.Pow((V[i].x - e.X), 2) + Math.Pow((V[i].y - e.Y), 2) <= G.R * G.R)
                    {
                        for (int j = 0; j < E.Count; j++)
                        {
                            if ((E[j].v1 == i) || (E[j].v2 == i))
                            {
                                E.RemoveAt(j);
                                j--;
                            }
                            else
                            {
                                if (E[j].v1 > i) E[j].v1--;
                                if (E[j].v2 > i) E[j].v2--;
                            }
                        }
                        V.RemoveAt(i);
                        flag = true;
                        break;
                    }
                }
                //ищем, возможно было нажато ребро
                if (!flag)
                {
                    for (int i = 0; i < E.Count; i++)
                    {
                        if (E[i].v1 == E[i].v2) //если это петля
                        {
                            if ((Math.Pow((V[E[i].v1].x - G.R - e.X), 2) + Math.Pow((V[E[i].v1].y - G.R - e.Y), 2) <= ((G.R + 2) * (G.R + 2))) &&
                                (Math.Pow((V[E[i].v1].x - G.R - e.X), 2) + Math.Pow((V[E[i].v1].y - G.R - e.Y), 2) >= ((G.R - 2) * (G.R - 2))))
                            {
                                E.RemoveAt(i);
                                flag = true;
                                break;
                            }
                        }
                        else //не петля
                        {
                            if (((e.X - V[E[i].v1].x) * (V[E[i].v2].y - V[E[i].v1].y) / (V[E[i].v2].x - V[E[i].v1].x) + V[E[i].v1].y) <= (e.Y + 4) &&
                                ((e.X - V[E[i].v1].x) * (V[E[i].v2].y - V[E[i].v1].y) / (V[E[i].v2].x - V[E[i].v1].x) + V[E[i].v1].y) >= (e.Y - 4))
                            {
                                if ((V[E[i].v1].x <= V[E[i].v2].x && V[E[i].v1].x <= e.X && e.X <= V[E[i].v2].x) ||
                                    (V[E[i].v1].x >= V[E[i].v2].x && V[E[i].v1].x >= e.X && e.X >= V[E[i].v2].x))
                                {
                                    E.RemoveAt(i);
                                    flag = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                //если что-то было удалено, то обновляем граф на экране
                if (flag)
                {
                    G.clearSheet();
                    G.drawALLGraph(V, E);
                    sheet.Image = G.GetBitmap();
                }
            }
        }

        //создание матрицы смежности и вывод в листбокс
        private void createAdjAndOut()
        {
            AMatrix = new int[V.Count, V.Count];
            G.fillAdjacencyMatrix(V.Count, E, AMatrix);
            listBoxMatrix.Items.Clear();
            string sOut = "    ";
            for (int i = 0; i < V.Count; i++)
                sOut += (i + 1) + " ";
            listBoxMatrix.Items.Add(sOut);
            for (int i = 0; i < V.Count; i++)
            {
                sOut = (i + 1) + " | ";
                for (int j = 0; j < V.Count; j++)
                    sOut += AMatrix[i, j] + " ";
                listBoxMatrix.Items.Add(sOut);
            }
        }

        //создание матрицы инцидентности и вывод в листбокс
        private void createIncAndOut()
        {
            if (E.Count > 0)
            {
                IMatrix = new int[V.Count, E.Count];
                G.fillIncidenceMatrix(V.Count, E, IMatrix);
                listBoxMatrix.Items.Clear();
                string sOut = "    ";
                for (int i = 0; i < E.Count; i++)
                    sOut += E[i].Name + " ";
                listBoxMatrix.Items.Add(sOut);
                for (int i = 0; i < V.Count; i++)
                {
                    sOut = (i + 1) + " | ";
                    for (int j = 0; j < E.Count; j++)
                        sOut += IMatrix[i, j] + " ";
                    listBoxMatrix.Items.Add(sOut);
                }
            }
            else
                listBoxMatrix.Items.Clear();
        }

        //поиск элементарных цепей
        private void chainButton_Click(object sender, EventArgs e)
        {
            listBoxMatrix.Items.Clear();
            //1-white 2-black
            int[] color = new int[V.Count];
            for (int i = 0; i < V.Count - 1; i++)
                for (int j = i + 1; j < V.Count; j++)
                {
                    for (int k = 0; k < V.Count; k++)
                        color[k] = 1;
                    DFSchain(i, j, E, color, (i + 1).ToString());
                }
        }

        //обход в глубину. поиск элементарных цепей. (1-white 2-black)
        private void DFSchain(int u, int endV, List<Edge> E, int[] color, string s)
        {
            //вершину не следует перекрашивать, если u == endV (возможно в нее есть несколько путей)
            if (u != endV)  
                color[u] = 2;
            else
            {
                listBoxMatrix.Items.Add(s);
                return;
            }
            for (int w = 0; w < E.Count; w++)
            {
                if (color[E[w].v2] == 1 && E[w].v1 == u)
                {
                    DFSchain(E[w].v2, endV, E, color, s + "-" + (E[w].v2+1).ToString());
                    color[E[w].v2] = 1;
                }
                else if (color[E[w].v1] == 1 && E[w].v2 == u)
                {
                    DFSchain(E[w].v1, endV, E, color, s + "-" + (E[w].v1+1).ToString());
                    color[E[w].v1] = 1;
                }
            }
        }

        //поиск элементарных циклов
        private void cycleButton_Click(object sender, EventArgs e)
        {
            listBoxMatrix.Items.Clear();
            //1-white 2-black
            int[] color = new int[V.Count];
            for (int i = 0; i < V.Count; i++)
            {
                for (int k = 0; k < V.Count; k++)
                    color[k] = 1;
                List<int> cycle = new List<int>();
                cycle.Add(i + 1);
                DFScycle(i, i, E, color, -1, cycle);
            }
        }

        //обход в глубину. поиск элементарных циклов. (1-white 2-black)
        //Вершину, для которой ищем цикл, перекрашивать в черный не будем. Поэтому, для избежания неправильной
        //работы программы, введем переменную unavailableEdge, в которой будет хранится номер ребра, исключаемый
        //из рассмотрения при обходе графа. В действительности это необходимо только на первом уровне рекурсии,
        //чтобы избежать вывода некорректных циклов вида: 1-2-1, при наличии, например, всего двух вершин.

        private void DFScycle(int u, int endV, List<Edge> E, int[] color, int unavailableEdge, List<int> cycle)
        {
            //если u == endV, то эту вершину перекрашивать не нужно, иначе мы в нее не вернемся, а вернуться необходимо
            if (u != endV)
                color[u] = 2;
            else
            {
                if (cycle.Count >= 2)
                {
                    cycle.Reverse();
                    string s = cycle[0].ToString();
                    for (int i = 1; i < cycle.Count; i++)
                        s += "-" + cycle[i].ToString();
                    bool flag = false; //есть ли палиндром для этого цикла графа в листбоксе?
                    for (int i = 0; i < listBoxMatrix.Items.Count; i++)
                        if (listBoxMatrix.Items[i].ToString() == s)
                        {
                            flag = true;
                            break;
                        }
                    if (!flag)
                    {
                        cycle.Reverse();
                        s = cycle[0].ToString();
                        for (int i = 1; i < cycle.Count; i++)
                            s += "-" + cycle[i].ToString();
                        listBoxMatrix.Items.Add(s);
                    }
                    return;
                }
            }
            for (int w = 0; w < E.Count; w++)
            {
                if (w == unavailableEdge)
                    continue;
                if (color[E[w].v2] == 1 && E[w].v1 == u)
                {
                    List<int> cycleNEW = new List<int>(cycle);
                    cycleNEW.Add(E[w].v2+1);
                    DFScycle(E[w].v2, endV, E, color, w, cycleNEW);
                    color[E[w].v2] = 1;
                }
                else if (color[E[w].v1] == 1 && E[w].v2 == u)
                {
                    List<int> cycleNEW = new List<int>(cycle);
                    cycleNEW.Add(E[w].v1+1);
                    DFScycle(E[w].v1, endV, E, color, w, cycleNEW);
                    color[E[w].v1] = 1;
                }
            }
        }

        //О программе
        private void about_Click(object sender, EventArgs e) => new aboutForm().ShowDialog();

        private void SaveToXML_Click(object sender, EventArgs e)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(Graph));
            if (File.Exists("graph.xml"))
                File.Delete("graph.xml");
            using (FileStream fs = new FileStream("graph.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, new Graph
                {
                    V = V,
                    E = E
                });
            }
            MessageBox.Show("Сохранено в ./graph.xml");
        }

        private void ImportXML_Click(object sender, EventArgs e)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(Graph));
            using (FileStream fs = new FileStream("graph.xml", FileMode.OpenOrCreate))
            {
                Graph graph = (Graph)formatter.Deserialize(fs);
                V = graph.V;
                E = graph.E;
                G.clearSheet();
                G.drawALLGraph(V, E);
                selected1 = -1;
                selected2 = -1;
                sheet.Image = G.GetBitmap();
            }

            MessageBox.Show("Загружено из ./graph.xml");
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (sheet.Image != null)
            {
                SaveFileDialog savedialog = new SaveFileDialog()
                {
                    Title = "Сохранить картинку как...",
                    OverwritePrompt = true,
                    CheckPathExists = true,
                    Filter = "Image Files(*.BMP)|*.BMP|Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*",
                    ShowHelp = true
                };
                if (savedialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        sheet.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void Button1_Click(object sender, EventArgs e) => GenerateMPI();

        private void Form1_Load(object sender, EventArgs e)
        {
            ToolTip tchainButton = new ToolTip();
            tchainButton.SetToolTip(chainButton, "Поиск элементарных цепей");
            ToolTip tcycleButton = new ToolTip();
            tcycleButton.SetToolTip(cycleButton, "Поиск элементарных циклов");
        }

        public void GenerateMPI() 
        {
            StreamWriter sw = new StreamWriter("./MPI.cpp", false);
            List<string> neededLibs = new List<string>() { "mpi.h", "iostream", "!windows.h", "queue", "random", "algorithm", "iterator", "clocale", "vector", "map", "string" };
            for (int i = 0; i < neededLibs.Count; i++) 
            {
                string includeLine = "#include ";
                includeLine += neededLibs[i][0] == '!' ? $"\"{neededLibs[i].Replace("!", "")}\"" : $"<{neededLibs[i]}>";
                sw.WriteLine(includeLine);
            }
            sw.WriteLine("");
            sw.WriteLine("//КАРТА ПЕРЕХОДОВ АВТОМАТА НДКА");
            var E_pairs = new List<string>();
            Dictionary<string, Tuple<List<int>, List<int>>> vertex_IN_OUT = new Dictionary<string, Tuple<List<int>, List<int>>>(); //карта входных и выходных вершин (ключ - вершина, Item1 - IN вершины, Item2 - OUT вершины)
            for (int i = 0; i < V.Count; i++) 
            {
                vertex_IN_OUT.Add(V[i].name, new Tuple<List<int>, List<int>>(new List<int>(), new List<int>())); //добавляем вершину в справочник
            }
            var allEdges = new List<int[]>(); //e[v1][v2]
            for (int i = 0; i < E.Count; i++) 
            {
                E_pairs.Add($"{{ \"{E[i].EdgeKey}\", {{{E[i].v1},{E[i].v2}}} }}");
                allEdges.Add(new int[] { E[i].v1, E[i].v2 });
                if (E[i].IsDuplexRoute)
                {
                    E_pairs.Add($"{{ \"{E[i].ReversedEdgeKey}\", {{{E[i].v2},{E[i].v1}}} }}");
                    allEdges.Add(new int[] { E[i].v2, E[i].v1 });
                }
            }
            foreach (var pair in vertex_IN_OUT) 
            {
                pair.Value.Item1.AddRange(allEdges.Where(item => item[1].ToString() == pair.Key).Select(item => item[0]).Distinct()); //IN
                pair.Value.Item2.AddRange(allEdges.Where(item => item[0].ToString() == pair.Key).Select(item => item[1]).Distinct()); //OUT
            }
            sw.WriteLine("std::map<std::string, std::vector<int>> routes"+ (E_pairs.Count > 0 ? " = { " + string.Join(",", E_pairs) : "") + " };");
            sw.WriteLine("int RANK = 0;");
            sw.WriteLine("");
            sw.WriteLine("//ФЛАГИ БЛОКИРОВКИ ВЕРШИНЫ");
            sw.WriteLine("bool isLock = false; //флаг блокировки 1");
            sw.WriteLine("bool isSpecLock = false; //флаг блокировки 2");
            sw.WriteLine("int LVertexNumber = -1; //разрешающая вершина в блокировке 1");
            sw.WriteLine("bool EvenVertexLockSelector = false; //разрешающие вершины по четности в блокировке 2");
            sw.WriteLine("");
            sw.WriteLine("//ФУНКЦИЯ ПЕРЕХОДА В СЛЕДУЮЩУЮ ВЕРШИНУ, пример: GoToVertex(\"e23\");");
            sw.WriteLine("void GoToVertex(std::string edge_or_key)");
            sw.WriteLine("{");
            sw.WriteLine("\tstd::map<std::string, std::vector<int>>::iterator it = routes.find(edge_or_key);");
            sw.WriteLine("\tif (it != routes.end() && RANK == it->second[0])");
            sw.WriteLine("\t{");
            sw.WriteLine("\t\tint op = 1;");
            sw.WriteLine("\t\tMPI_Send(&op, 1, MPI_INT, it->second[1], 0, MPI_COMM_WORLD);");
            sw.WriteLine("\t}");
            sw.WriteLine("}");
            sw.WriteLine("");
            sw.WriteLine("//ФУНКЦИЯ ПЕРЕХОДА В СЛЕДУЮЩУЮ(ИЕ) ВЕРШИНУ(Ы)");
            sw.WriteLine("void GoToVertexes(std::vector<std::string> edges_or_keys)");
            sw.WriteLine("{");
            sw.WriteLine("\tMPI_Request* reqs = new MPI_Request[edges_or_keys.size()];");
            sw.WriteLine("\tMPI_Status* statuses = new MPI_Status[edges_or_keys.size()];");
            sw.WriteLine("\tfor (int i = 0; i < edges_or_keys.size(); i++)");
            sw.WriteLine("\t{");
            sw.WriteLine("\t\tstd::map<std::string, std::vector<int>>::iterator it = routes.find(edges_or_keys[i]);");
            sw.WriteLine("\t\tif (it != routes.end() && RANK == it->second[0])");
            sw.WriteLine("\t\t{");
            sw.WriteLine("\t\t\tint op = 1;");
            sw.WriteLine("\t\t\tMPI_Isend(&op, 1, MPI_INT, it->second[1], 0, MPI_COMM_WORLD, &reqs[i]);");
            sw.WriteLine("\t\t}");
            sw.WriteLine("\t}");
            sw.WriteLine("\tMPI_Waitall(edges_or_keys.size(), reqs, statuses);");
            sw.WriteLine("}");
            sw.WriteLine("");
            sw.WriteLine("//ПРОВЕРКА ДОСТУПА ПО ФЛАГУ БЛОКИРОВКИ 1");
            sw.WriteLine("bool IsAllow(int source)");
            sw.WriteLine("{");
            sw.WriteLine("\treturn isLock && LVertexNumber == source;");
            sw.WriteLine("}");
            sw.WriteLine("");
            sw.WriteLine("//ПРОВЕРКА ДОСТУПА ПО ФЛАГУ БЛОКИРОВКИ 2");
            sw.WriteLine("bool IsSpecAllow(int source)");
            sw.WriteLine("{");
            sw.WriteLine("\treturn isSpecLock && source % 2 == (EvenVertexLockSelector ? 0 : 1);");
            sw.WriteLine("}");
            sw.WriteLine("");
            sw.WriteLine("//ФУНКЦИЯ ПРОВЕРКИ БЛОКИРОВКИ");
            sw.WriteLine("bool CheckLock(MPI_Status st)");
            sw.WriteLine("{");
            sw.WriteLine("\tint op = 0;");
            sw.WriteLine("\tif (st.MPI_TAG == 0xFC)");
            sw.WriteLine("\t{");
            sw.WriteLine("\t\tif (isLock)");
            sw.WriteLine("\t\t\tMPI_Send(&op, 0, MPI_INT, st.MPI_SOURCE, IsAllow(st.MPI_SOURCE) ? 0xFE : 0xFD, MPI_COMM_WORLD);");
            sw.WriteLine("\t\telse if (isSpecLock)");
            sw.WriteLine("\t\t\tMPI_Send(&op, 0, MPI_INT, st.MPI_SOURCE, IsSpecAllow(st.MPI_SOURCE) ? 0xFE : 0xFD, MPI_COMM_WORLD);");
            sw.WriteLine("\t\telse");
            sw.WriteLine("\t\t\tMPI_Send(&op, 0, MPI_INT, st.MPI_SOURCE, 0xFE, MPI_COMM_WORLD);");
            sw.WriteLine("\t\treturn true;");
            sw.WriteLine("\t}");
            sw.WriteLine("\treturn false;");
            sw.WriteLine("}");
            sw.WriteLine("");
            sw.WriteLine("//ФУНКЦИЯ УСТАНОВКИ КРИТИЧЕСКОЙ СЕКЦИИ");
            sw.WriteLine("void LockOn(int vertex)");
            sw.WriteLine("{");
            sw.WriteLine("\tisLock = true;");
            sw.WriteLine("\tLVertexNumber = vertex;");
            sw.WriteLine("}");
            sw.WriteLine("");
            sw.WriteLine("//ФУНКЦИЯ СНЯТИЯ КРИТИЧЕСКОЙ СЕКЦИИ");
            sw.WriteLine("void LockOff()");
            sw.WriteLine("{");
            sw.WriteLine("\tisLock = false;");
            sw.WriteLine("\tLVertexNumber = -1;");
            sw.WriteLine("}");
            sw.WriteLine("");
            sw.WriteLine("//ФУНКЦИЯ БЛОКИРОВКИ НЕЧЕТНЫХ И ЧЕТНЫХ ВЕРШИН, lock [устанановка через T, снятие - F], isEvenVertexLock [T - четные, F - нечетные]");
            sw.WriteLine("void SpecLock(bool lock, bool isEvenVertexLock)");
            sw.WriteLine("{");
            sw.WriteLine("\tisSpecLock = lock;");
            sw.WriteLine("\tEvenVertexLockSelector = isEvenVertexLock;");
            sw.WriteLine("}");
            sw.WriteLine("");
            sw.WriteLine("//ПРОВЕРКА НА НАЛИЧИЕ УСТАНОВЛЕННОЙ КРИТИЧЕСКОЙ СЕКЦИИ В ВЕРШИНЕ");
            sw.WriteLine("bool IsVertexEnable(std::string edge_or_key)");
            sw.WriteLine("{");
            sw.WriteLine("\tstd::map<std::string, std::vector<int>>::iterator it = routes.find(edge_or_key);");
            sw.WriteLine("\tif (it != routes.end() && RANK == it->second[0])");
            sw.WriteLine("\t{");
            sw.WriteLine("\t\tint op = 1;");
            sw.WriteLine("\t\tMPI_Status st;");
            sw.WriteLine("\t\tMPI_Send(&op, 0, MPI_INT, it->second[1], 0xFC, MPI_COMM_WORLD);");
            sw.WriteLine("\t\tMPI_Recv(&op, 0, MPI_INT, it->second[1], MPI_ANY_TAG, MPI_COMM_WORLD, &st);");
            sw.WriteLine("\t\treturn st.MPI_TAG == 0xFE;");
            sw.WriteLine("\t}");
            sw.WriteLine("\treturn false;");
            sw.WriteLine("}");
            sw.WriteLine("");
            sw.WriteLine("//ФУНКЦИЯ ОЖИДАНИЯ");
            sw.WriteLine("MPI_Status WaitVertex()");
            sw.WriteLine("{");
            sw.WriteLine("\tMPI_Status st;");
            sw.WriteLine("\twhile (1)");
            sw.WriteLine("\t{");
            sw.WriteLine("\t\tint op = 0;");
            sw.WriteLine("\t\tMPI_Recv(&op, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &st); ");
            sw.WriteLine("\t\tif (CheckLock(st) || (isLock && !IsAllow(st.MPI_SOURCE)) || (isSpecLock && !IsSpecAllow(st.MPI_SOURCE)))");
            sw.WriteLine("\t\t\tcontinue;");
            //создаем таблицу входных вершин
            var waitVertexMap_ifelse = new List<string>();
            foreach (var pair in vertex_IN_OUT) 
            {
                if (pair.Value.Item1.Count > 0)
                {
                    var sources = new List<string>();
                    foreach (var input in pair.Value.Item1)
                        sources.Add($"st.MPI_SOURCE == {input}");
                    waitVertexMap_ifelse.Add($"(RANK == {pair.Key} && ({string.Join(" || ", sources)}))");
                }
            }
            if (waitVertexMap_ifelse.Count > 0)
            {
                sw.WriteLine($"\t\tif ({string.Join(" || ", waitVertexMap_ifelse)})");
                sw.WriteLine("\t\t{");
                sw.WriteLine("\t\t\tbreak;");
                sw.WriteLine("\t\t}");
            }
            sw.WriteLine("\t}");
            sw.WriteLine("\treturn st;");
            sw.WriteLine("}");
            sw.WriteLine("");
            sw.WriteLine("//ТОЧКА ВХОДА");
            sw.WriteLine("int main(int argc, char** argv)");
            sw.WriteLine("{");
            sw.WriteLine("\tint SIZE; //количество вершин");
            sw.WriteLine("\tMPI_Init(&argc, &argv);");
            sw.WriteLine("\tMPI_Comm_size(MPI_COMM_WORLD, &SIZE);");
            sw.WriteLine("\tMPI_Comm_rank(MPI_COMM_WORLD, &RANK);");
            sw.WriteLine("\tMPI_Barrier(MPI_COMM_WORLD); //синхронизация всех потоков");
            sw.WriteLine("");
            sw.WriteLine("\twhile(1)");
            /*
            нужно сделать кнопку что должно быть в системном потоке... т.е. подумать над организацией системного потока
            
            нужна сериализация данных, при отправке сложных объектов - я предлагаю std::string и std::vector
            ------------------------
            // Sender
            string bla = "blabla";
            MPI::COMM_WORLD.Send(bla.c_str(), bla.length(), MPI::CHAR, dest, 1);
            // Receiver
            MPI::Status status;
            MPI::COMM_WORLD.Probe(source, 1, status);
            int l = status.Get_count(MPI::CHAR);
            char *buf = new char[l];
            MPI::COMM_WORLD.Recv(buf, l, MPI::CHAR, source, 1, status);
            string bla1(buf, l);
            delete [] buf;
            -----------------------------------
            std::vector<uint32_t> m_image_data2;
            if (rank==0) {
              m_image_data2.push_back(1);
              m_image_data2.push_back(2);
              m_image_data2.push_back(3);
              m_image_data2.push_back(4);
              m_image_data2.push_back(5);
              // send 5 ints at once
              MPI_Send( &m_image_data2[0], 5, MPI_INT, 1, 0, MPI_COMM_WORLD);
            }
            else {
              // make space for 5 ints
              m_image_data2.resize(5);
              // receive 5 ints
              MPI_Recv(&m_image_data2[0], 5, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
            }
            -----------------------*/
            sw.WriteLine("\t{");
            for (int vertexesIndex = 0; vertexesIndex < V.Count; vertexesIndex++) 
            {
                var vertexINOUTone = vertex_IN_OUT[V[vertexesIndex].name];
                sw.WriteLine("\t\t"+ (vertexesIndex == 0 ? "if" : "else if") +" (RANK == " + V[vertexesIndex].name + ")");
                sw.WriteLine("\t\t{");
                sw.WriteLine("\t\t\t//КОД ВЕРШИНЫ");
                if (V[vertexesIndex].name == "0")
                {
                    //сначала senders
                    if (vertexINOUTone.Item2.Count > 0) 
                    {
                        for (int i = 0; i < vertexINOUTone.Item2.Count; i++)
                        {
                            sw.WriteLine($"\t\t\t//if (IsVertexEnable(\"e{V[vertexesIndex].name}{vertexINOUTone.Item2[i]}\"))");
                            sw.WriteLine($"\t\t\t//\t  GoToVertex(\"e{V[vertexesIndex].name}{vertexINOUTone.Item2[i]}\");");
                        }
                    }
                    //затем waiters
                    if (vertexINOUTone.Item1.Count > 0)
                        sw.WriteLine($"\t\t\t//WaitVertex();");
                }
                else
                {
                    //сначала waiters
                    if (vertexINOUTone.Item1.Count > 0)
                        sw.WriteLine($"\t\t\t//WaitVertex();");
                    //затем senders
                    if (vertexINOUTone.Item2.Count > 0)
                    {
                        for (int i = 0; i < vertexINOUTone.Item2.Count; i++)
                        {
                            sw.WriteLine($"\t\t\t//if (IsVertexEnable(\"e{V[vertexesIndex].name}{vertexINOUTone.Item2[i]}\"))");
                            sw.WriteLine($"\t\t\t//\t  GoToVertex(\"e{V[vertexesIndex].name}{vertexINOUTone.Item2[i]}\");");
                        }
                    }
                }
                //sw.WriteLine("\t\t\t" + V[vertexesIndex].pcode);
                sw.WriteLine("\t\t}");
            }
            sw.WriteLine("\t}");
            sw.WriteLine("\tMPI_Finalize();");
            sw.WriteLine("\treturn 0;");
            sw.WriteLine("}");
            sw.Close();
            MessageBox.Show("Сохранено в ./MPI.cpp");
        }
    }
}
