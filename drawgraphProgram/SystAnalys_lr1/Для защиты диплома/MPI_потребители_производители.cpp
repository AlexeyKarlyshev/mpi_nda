#include <mpi.h>
#include <iostream>
#include "windows.h"
#include <queue>
#include <random>
#include <algorithm>
#include <iterator>
#include <clocale>
#include <vector>
#include <map>
#include <string>

//КАРТА ПЕРЕХОДОВ АВТОМАТА НДКА
std::map<std::string, std::vector<int>> routes = { { "e10", {1,0} },{ "e01", {0,1} },{ "e02", {0,2} },{ "e20", {2,0} } };
int RANK = 0;

//ФЛАГИ БЛОКИРОВКИ ВЕРШИНЫ
bool isLock = false; //флаг блокировки 1
bool isSpecLock = false; //флаг блокировки 2
int LVertexNumber = -1; //разрешающая вершина в блокировке 1
bool EvenVertexLockSelector = false; //разрешающие вершины по четности в блокировке 2

//ФУНКЦИЯ ПЕРЕХОДА В СЛЕДУЮЩУЮ ВЕРШИНУ, пример: GoToVertex("e23");
void GoToVertex(std::string edge_or_key)
{
	std::map<std::string, std::vector<int>>::iterator it = routes.find(edge_or_key);
	if (it != routes.end())
	{
		int op = 1;
		MPI_Send(&op, 1, MPI_INT, it->second[1], 0, MPI_COMM_WORLD);
		
	}
}

//ФУНКЦИЯ ПЕРЕХОДА В СЛЕДУЮЩУЮ(ИЕ) ВЕРШИНУ(Ы)
void GoToVertexes(std::vector<std::string> edges_or_keys)
{
	MPI_Request* reqs = new MPI_Request[edges_or_keys.size()];
	MPI_Status* statuses = new MPI_Status[edges_or_keys.size()];
	for (int i = 0; i < edges_or_keys.size(); i++)
	{
		std::map<std::string, std::vector<int>>::iterator it = routes.find(edges_or_keys[i]);
		if (it != routes.end() && RANK == it->second[0])
		{
			int op = 1;
			MPI_Isend(&op, 1, MPI_INT, it->second[1], 0, MPI_COMM_WORLD, &reqs[i]);
		}
	}
	MPI_Waitall(edges_or_keys.size(), reqs, statuses);
}

//ПРОВЕРКА ДОСТУПА ПО ФЛАГУ БЛОКИРОВКИ 1
bool IsAllow(int source)
{
	return isLock && LVertexNumber == source;
}

//ПРОВЕРКА ДОСТУПА ПО ФЛАГУ БЛОКИРОВКИ 2
bool IsSpecAllow(int source)
{
	return isSpecLock && (source % 2 == (EvenVertexLockSelector ? 0 : 1));
}

//ФУНКЦИЯ ПРОВЕРКИ БЛОКИРОВКИ
bool CheckLock(MPI_Status st)
{
	int op = 0;
	if (st.MPI_TAG == 0xFC)
	{
		if (isLock)
			MPI_Send(&op, 0, MPI_INT, st.MPI_SOURCE, IsAllow(st.MPI_SOURCE) ? 0xFE : 0xFD, MPI_COMM_WORLD);
		else if (isSpecLock)
			MPI_Send(&op, 0, MPI_INT, st.MPI_SOURCE, IsSpecAllow(st.MPI_SOURCE) ? 0xFE : 0xFD, MPI_COMM_WORLD);
		else
			MPI_Send(&op, 0, MPI_INT, st.MPI_SOURCE, 0xFE, MPI_COMM_WORLD);
		return true;
	}
	return false;
}

//ФУНКЦИЯ УСТАНОВКИ КРИТИЧЕСКОЙ СЕКЦИИ
void LockOn(int vertex)
{
	isLock = true;
	LVertexNumber = vertex;
}

//ФУНКЦИЯ СНЯТИЯ КРИТИЧЕСКОЙ СЕКЦИИ
void LockOff()
{
	isLock = false;
	LVertexNumber = -1;
}

//ФУНКЦИЯ БЛОКИРОВКИ НЕЧЕТНЫХ И ЧЕТНЫХ ВЕРШИН, lock [устанановка через T, снятие - F], isEvenVertexLock [T - четные, F - нечетные]
void SpecLock(bool lock, bool isEvenVertexLock)
{
	isSpecLock = lock;
	EvenVertexLockSelector = isEvenVertexLock;
}

//ПРОВЕРКА НА НАЛИЧИЕ УСТАНОВЛЕННОЙ КРИТИЧЕСКОЙ СЕКЦИИ В ВЕРШИНЕ
bool IsVertexEnable(std::string edge_or_key)
{
	std::map<std::string, std::vector<int>>::iterator it = routes.find(edge_or_key);
	if (it != routes.end())
	{
		int op = 1;
		MPI_Status st;
		MPI_Send(&op, 0, MPI_INT, it->second[1], 0xFC, MPI_COMM_WORLD);
		MPI_Recv(&op, 0, MPI_INT, it->second[1], MPI_ANY_TAG, MPI_COMM_WORLD, &st);
		return st.MPI_TAG == 0xFE;
	}
	return false;
}

//ФУНКЦИЯ ОЖИДАНИЯ
MPI_Status WaitVertex()
{
	MPI_Status st;
	while (1)
	{
		int op = 0;
		MPI_Recv(&op, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &st);
		if (CheckLock(st) || (isLock && !IsAllow(st.MPI_SOURCE)) || (isSpecLock && !IsSpecAllow(st.MPI_SOURCE)))
			continue;
		if (RANK == 0 || st.MPI_SOURCE == 0)
		{
			break;
		}
	}
	return st;
}

//ТОЧКА ВХОДА
int main(int argc, char** argv)
{
	int SIZE; //количество вершин
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &SIZE);
	MPI_Comm_rank(MPI_COMM_WORLD, &RANK);
	MPI_Barrier(MPI_COMM_WORLD); //синхронизация всех потоков
	int warehouse = 0, max_warehouse = 15;

	while (1)
	{
		if (RANK == 0)
		{
			if (warehouse == 0)
				SpecLock(true, false); //только производителям
			else if (warehouse == max_warehouse)
				SpecLock(true, true); //только потребители
			else
				SpecLock(false, false); //снятие спец.блокировки
			MPI_Status st_wait = WaitVertex();
			bool PC = st_wait.MPI_SOURCE % 2 == 1; //true - производители, false - потребители
			if (PC) //производители
			{
				if (warehouse < max_warehouse)
					warehouse++;
			}
			else
			{
				if (warehouse > 0)
					warehouse--;
			}
			std::cout << "VERTEX: " << st_wait.MPI_SOURCE << "; Type = " << (PC ? "P" : "C") << ";warehouse = " 
					  << warehouse << "/" << max_warehouse << std::endl;
			int op = 1;
			MPI_Send(&op, 1, MPI_INT, st_wait.MPI_SOURCE, 0, MPI_COMM_WORLD);
		}
		else if (RANK % 2 == 1) //производители
		{
			while (!IsVertexEnable("e10")) {}
			GoToVertex("e10");
			WaitVertex();
			std::cout << "Vertex " << RANK << " added item to warehouse" << std::endl;
			Sleep(1000);
		}
		else if (RANK % 2 == 0) //потребители
		{
			while (!IsVertexEnable("e20")) {}
			GoToVertex("e20");
			WaitVertex();
			std::cout << "Vertex " << RANK << " took item from warehouse" << std::endl;
			Sleep(700);
		}
	}
	MPI_Finalize();
	return 0;
}
