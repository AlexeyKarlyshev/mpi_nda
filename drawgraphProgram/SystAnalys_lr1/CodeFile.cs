﻿using System.Collections.Generic;
using System.Drawing;

namespace SystAnalys_lr1
{
    class DrawGraph
    {
        Bitmap bitmap;
        Pen blackPen, redPen, darkGoldPen;
        Graphics gr;
        Font fo;
        Brush br;
        PointF point;
        public int R = 20; //радиус окружности вершины

        public DrawGraph(int width, int height)
        {
            bitmap = new Bitmap(width, height);
            gr = Graphics.FromImage(bitmap);
            clearSheet();
            blackPen = new Pen(Color.Black) { Width = 2 };
            redPen = new Pen(Color.Red) { Width = 2 };
            darkGoldPen = new Pen(Color.DarkGoldenrod) { Width = 2 };
            fo = new Font("Arial", 15);
            br = Brushes.Black;
        }

        public Bitmap GetBitmap() => bitmap;

        public void clearSheet() => gr.Clear(Color.White);

        public void drawVertex(int x, int y, string number)
        {
            gr.FillEllipse(Brushes.White, (x - R), (y - R), 2 * R, 2 * R);
            gr.DrawEllipse(blackPen, (x - R), (y - R), 2 * R, 2 * R);
            point = new PointF(x - 9, y - 9);
            gr.DrawString(number, fo, br, point);
        }

        public void drawSelectedVertex(int x, int y) => gr.DrawEllipse(redPen, (x - R), (y - R), 2 * R, 2 * R);

        public void drawEdge(Vertex V1, Vertex V2, Edge E, bool isDrawVertexes = true)
        {
            if (E.v1 == E.v2) //закольцовка
            {
                gr.DrawArc(darkGoldPen, (V1.x - 2 * R), (V1.y - 2 * R), 2 * R, 2 * R, 90, 270);
                point = new PointF(V1.x - (int)(2.75 * R), V1.y - (int)(2.75 * R));
                gr.DrawString(E.Name, fo, br, point);
                if (isDrawVertexes)
                    drawVertex(V1.x, V1.y, E.v1.ToString());
            }
            else //иначе, от вершины 1 к вершине 2
            {
                gr.DrawLine(darkGoldPen, V1.x, V1.y, V2.x, V2.y);
                point = new PointF((V1.x + V2.x) / 2, (V1.y + V2.y) / 2);
                gr.DrawString(E.Name, fo, br, point);
                if (isDrawVertexes)
                {
                    drawVertex(V1.x, V1.y, E.v1.ToString());
                    drawVertex(V2.x, V2.y, E.v2.ToString());
                }
            }
        }

        public void drawALLGraph(List<Vertex> V, List<Edge> E)
        {
            //рисуем ребра
            for (int i = 0; i < E.Count; i++)
                drawEdge(V[E[i].v1], V[E[i].v2], E[i], false);
            //рисуем вершины
            for (int i = 0; i < V.Count; i++)
                drawVertex(V[i].x, V[i].y, V[i].name);
        }

        //заполняет матрицу смежности
        public void fillAdjacencyMatrix(int numberV, List<Edge> E, int[,] matrix)
        {
            for (int i = 0; i < numberV; i++)
                for (int j = 0; j < numberV; j++)
                    matrix[i, j] = 0;
            for (int i = 0; i < E.Count; i++)
            {
                matrix[E[i].v1, E[i].v2] = 1;
                matrix[E[i].v2, E[i].v1] = 1;
            }
        }

        //заполняет матрицу инцидентности
        public void fillIncidenceMatrix(int numberV, List<Edge> E, int[,] matrix)
        {
            for (int i = 0; i < numberV; i++)
                for (int j = 0; j < E.Count; j++)
                    matrix[i, j] = 0;
            for (int i = 0; i < E.Count; i++)
            {
                matrix[E[i].v1, i] = 1;
                matrix[E[i].v2, i] = 1;
            }
        }
    }
}