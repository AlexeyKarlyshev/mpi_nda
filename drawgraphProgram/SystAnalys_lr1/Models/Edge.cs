﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SystAnalys_lr1 
{
    [Serializable]
    public class Edge
    {
        public int v1, v2;
        private string generatedName = "e";

        public bool IsDuplexRoute { get; set; }

        /// <summary>
        /// Ключ ребра для вызова пути от v1 к v2
        /// </summary>
        public string EdgeKey => $"{generatedName}{v1}{v2}";

        public string Name => EdgeKey + (IsDuplexRoute ? ("," + ReversedEdgeKey) : "");

        /// <summary>
        /// Ключ ребра для вызова пути от v2 к v1
        /// </summary>
        public string ReversedEdgeKey => $"{generatedName}{v2}{v1}";

        public Edge(int v1, string v1_name, int v2, string v2_name)
        {
            this.v1 = v1; 
            this.v2 = v2;
        }

        public Edge() { }

        public bool CreateReverse(int rv1, int rv2)
        {
            if (IsDuplexRoute) return false;
            if (rv1 == v2 && rv2 == v1 && v1 != v2)
            {
                IsDuplexRoute = true;
                return true;
            }
            return false;
        }
    }
}