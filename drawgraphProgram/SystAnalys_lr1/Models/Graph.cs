﻿using System;
using System.Collections.Generic;

namespace SystAnalys_lr1 
{
    [Serializable]
    public class Graph
    {
        public List<Vertex> V { get; set; }
        public List<Edge> E { get; set; }
    }
}