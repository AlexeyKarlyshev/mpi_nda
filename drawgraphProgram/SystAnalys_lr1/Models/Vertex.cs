﻿using System;

namespace SystAnalys_lr1 
{
    [Serializable]
    public class Vertex
    {
        public int x, y;
        public string name;
        public string pcode;

        public Vertex(int x, int y, string name)
        {
            this.x = x;
            this.y = y;
            this.name = name;
            this.pcode = "";
        }

        public Vertex() { }
    }
}